angular.module('harman')
    .controller('mainController', function ($scope, $rootScope, $location, $ionicHistory, $timeout, $state, $ionicSideMenuDelegate) {
        $scope.showExpandCont = true;
        $scope.alertLoader = false;
        $scope.currentRout = function (path) {
            var loc = $location.path();
            return loc.includes(path);
        };

        $scope.openPopup = function (popup) {
            $(".popupBox").fadeOut(0).removeClass('open');
            $('.' + popup).fadeIn(0).addClass('open');
            $('body').scrollTop(0);
        };
        $scope.closePopup = function (popup, keepBackground) {
            $('.' + popup).fadeOut(0).removeClass('open');
        };
        $scope.showMessagePopup = function (popup) {
            $('.background').fadeIn(100);
            $('.' + popup).fadeIn(0).addClass('show_fade');
            $timeout(function () {
                $('.' + popup).fadeOut(300).removeClass('show_fade');
                $('.background').fadeOut(300);
            }, 5000);
        };

        $scope.expandContent = function () {
            $scope.showExpandCont = false;
            $timeout(function () {
                $scope.showExpandCont = true;
            }, 220);
        };

        $scope.doRefresh = function () {
            $timeout(function () {
                $scope.$broadcast('scroll.refreshComplete');
            }, 2000);
        }


        $scope.toggleScope={
            test: true
        };
        $scope.sampleValue=25;
        $scope.toggleFunc= function (val) {
        $scope.toggleScope[val]=!$scope.toggleScope[val];

        };
        $scope.mapDefaults = {
            scrollWheelZoom: false,
            attributionControl: false,
            zoomControl: false,
            tap: true,
            touchZoom: true,
            controls: {
                layers: {
                    visible: false,
                },
            },
        };

        $scope.appMap = {
            center: {
                lat: 12.985729,
                lng: 77.596314,
                zoom: 16
            },
            markers: [
                // {
                //   id: 0,
                //   name: 'Sample',
                //   lat: 0,
                //   lng: 0,
                //   compileMessage: true,
                //   getMessageScope: function () {
                //     return this;
                //   },
                //   label: {
                //     message: '<div class="liveMarker">' +
                //     '<ul class="instances">' +
                //     '<li class="green"><img src="img/WatchlistApp.png" alt=""></li>' +
                //     '<li class="green"><img src="img/PeopleCountApp.png" alt=""></li>'+
                //     '<span class="clear"></span>' +
                //     '</ul>' +
                //     '<div class="cameraName">Sensor A</div>' +
                //     '</div>',
                //     options: {
                //       noHide: true,
                //       offset: [22, -15],
                //     },
                //   },
                //   draggable: false,
                //   icon: {
                //     iconUrl: 'img/cam.png',
                //     iconSize: [33, 33],
                //   },
                //   popupOptions: {
                //     autoClose: false,
                //     closeButton: false,
                //     closeOnClick: false,
                //   }
                // }
            ],
            layers: {
                baselayers: {
                    googleRoadmap: {
                        name: 'Google Streets',
                        layerType: 'ROADMAP',
                        type: 'google',
                        layerOptions: {
                            mapOptions: {
                                // styles: $scope.mapType,
                            }
                        }
                    }
                }
            }
        };


        $scope.analytics = {
            sampleData: {
                x: ['12:00', '13:00', '14:00', '15:00', '16:00', '17:00', '18:00', '19:00'],
                y: [
                    {
                        name: 'All Cameras',
                        data: [90, 80, 70, 60, 50, 40, 30, 20]
                    },

                ],
            }
        };


        $scope.$watch(function () {
                return $ionicSideMenuDelegate.isOpenLeft();
            },
            function (isOpen) {
                if (isOpen) {
                    $(".menuList .item.active").addClass('animate');
                    console.log("open");
                } else {
                    $(".menuList .item").removeClass('animate');
                    console.log("close");
                }
            });

    });


