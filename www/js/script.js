/**
 * Created by MohammedSaleem on 12/11/15.
 */

$(document).ready(function () {
    let parentEle = $("body");
    fun = {
        dropDown: function () {
            parentEle.on("click", ".dropDown .head", function (e) {
                e.stopPropagation();
                let init = $(this).data("init");
                if (init == "0") {
                    $(".dropDown .list").slideUp(200);
                    $(".dropDown").removeClass("open");
                    $(".dropDown .head").data({
                        "init": "0"
                    });

                    $(this).parent().addClass("open");
                    $(this).parent().find(".list").slideDown(200);
                    $(this).data({
                        "init": "1"
                    })
                }
                else {
                    $(this).parent().find(".list").slideUp(200, function () {
                        $(this).parent().removeClass("open");
                    });
                    $(this).data({
                        "init": "0"
                    })
                }
            });
            parentEle.on("click", ".dropDown .list li", function (e) {
                let value = $(this).text();
                $(this).parents().eq(1).find(".headTitle").text(value);
            })
        },
        popup: function (btn, popup) {
            parentEle.on("click", btn, function (e) {
                e.stopPropagation();
                $(".popupBox").fadeOut(0).removeClass('open');
                $(popup).fadeIn(0).addClass('open');
                //$(".analyticsMain .notificationMain").fadeIn(0).removeClass("fadeOutLeftTime").addClass("fadeInLeftTime") ""
            });
            parentEle.on("click", ".popupBox .x, .popupBox .close", function (e) {
                setTimeout(function () {
                    $(".popupBox").fadeOut(0).removeClass('open');
                }, 200);
                //$(".analyticsMain .notificationMain").removeClass("fadeInLeftTime").addClass("fadeOutLeftTime");
                //setTimeout(function () {
                //    $(".analyticsMain .notificationMain").fadeOut(0);
                //},450);
            });
        },
        notificationClick: function () {
            parentEle.on('click', '.notificationList li', function (e) {
                e.stopPropagation();
                let init = $(this).data('init');

                if (init == '0') {
                    $(this).find('.actionBtn').slideDown(200);
                    $(this).data({
                        'init': '1',
                    });
                } else {
                    $(this).find('.actionBtn').slideUp(200);
                    $(this).data({
                        'init': '0',
                    });
                }
            });

            parentEle.on('click', '.notificationList li .actionBtn .btn', function (e) {
                e.stopPropagation();
                $('.notificationList li .actionBtn').slideUp(200);
                $('.notificationList li').data({
                    'init': '0',
                });
                $(this).parents().eq(2).addClass('notified');
            });
        },
        expandContent: function () {
            parentEle.on('click', '.expandableCont .expandHead', function (e) {
                let initVal=$(this).data('init');
                $('.expandableCont .expandHead').data({
                    init: "0"
                });
                $('.expandableCont').removeClass('active');
                $('.expandableCont .expandCont').slideUp(200);

                if (initVal=='0'){
                    $(this).data({
                        init: "1"
                    });
                    $(this).parent().addClass('active');
                    $(this).parent().find('.expandCont').slideDown(200);
                }
                else{
                    $(this).data({
                        init: "0"
                    });
                    $(this).parent().find('.expandCont').slideUp(300);
                    $(this).parent().removeClass('active');
                }

            })
        },
        toggleBtn: function () {
            parentEle.on("click",".toggleMain",function (e) {
                var init= $(this).data("init");
                if(init==0){
                    $(this).find(".option").removeClass("active");
                    $(this).find(".option.on").addClass("active");
                    $(this).data({
                        "init":"1"
                    });
                    $(this).find(".cover").animate({
                        marginLeft:"50%"
                    },150);
                    $(this).addClass('turnedOn')
                }
                else{
                    $(this).find(".option").removeClass("active");
                    $(this).find(".option.off").addClass("active");
                    $(this).data({
                        "init":"0"
                    });
                    $(this).find(".cover").animate({
                        marginLeft:"0"
                    },150);
                    $(this).removeClass('turnedOn')

                }
            })
        },
        defaultClick: function () {
            $(document).click(function () {
                $(".dropDown .list").slideUp(200, function () {
                    $(".dropDown").removeClass("open");
                });
                $(".dropDown .head").data({
                    "init": "0"
                });

                $('.expandableCont .expandHead').data({
                    init: "0"
                });
                $('.expandableCont').removeClass('active');
                $('.expandableCont .expandCont').slideUp(50);

                $(".background").fadeOut(100, function () {
                    $('.emergencyList').removeClass('animate');
                    $('.emergencyContBtnMain').data({
                        'init': '0'
                    });
                });
            })
        },
        preventDefaultClicks: function () {
            let preventEle = ".dropDown .list, .x, .expandableCont, .editActivity, .appDataBox .eventNum, .emergencyContBtnMain";
            parentEle.on("click", preventEle, function (e) {
                console.log("hi")
                e.stopImmediatePropagation();
                e.stopPropagation();
            });
            parentEle.on("click", '.x', function (e) {
                e.preventDefault();
            });
        }
    };
    fun.dropDown();
    fun.notificationClick();
    fun.expandContent();
    fun.toggleBtn();
    fun.defaultClick();
    fun.preventDefaultClicks();
});
